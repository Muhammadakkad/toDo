import axios from 'axios'

class TodoDataService {
    retrieveAllTodos() {
        return axios.get(`http://localhost:8080/jpa/todos`);
    }

        retrieveTodo(id) {
        return axios.get(`http://localhost:8080/jpa/todos/${id}`);
    }

    deleteTodo(id) {
        return axios.delete(`http://localhost:8080/jpa/todos/${id}`);
    }

    updateTodo(id,todo) {
        return axios.put(`http://localhost:8080/jpa/todos/${id}`,todo);
    }

    createTodo(todo) {
        return axios.post('http://localhost:8080/jpa/todos/', todo);
    }

}

export default new TodoDataService()