import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import TodoApp from './components/todo/TodoApp';
import './bootstrap.css'


ReactDOM.render(<TodoApp/>, document.getElementById('root'));

serviceWorker.unregister();
