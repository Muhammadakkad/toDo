import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import ListTodosComponent from "./ListTodosComponent.jsx";
import HeaderComponent from "./HeaderComponent.jsx";
import TodoComponent from "./TodoComponent.jsx";

class TodoApp extends Component {
  render() {
    return (
      <div className="TodoApp">
        <Router>
          <>
            <HeaderComponent />
            <Switch>
              <Route path="/todos/:id" component={TodoComponent} />
              <Route path="/todos" component={ListTodosComponent} />
            </Switch>
          </>
        </Router>
      </div>
    );
  }
}

export default TodoApp;
