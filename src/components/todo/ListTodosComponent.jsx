import React, { Component } from "react";
import TodoDataService from "../../api/todo/TodoDataService.js";
import moment from "moment";

class ListTodosComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      todos: [],
      message: null
    };
    
    this.deleteTodoClicked = this.deleteTodoClicked.bind(this);
    this.updateTodoClicked = this.updateTodoClicked.bind(this);
    this.addTodoClicked = this.addTodoClicked.bind(this);
    this.refreshTodos = this.refreshTodos.bind(this);
  }


  componentDidMount() {
    this.refreshTodos();
  }

  refreshTodos() {
    TodoDataService.retrieveAllTodos().then(response => {
      this.setState({ todos: response.data });
      
    });
  }

  deleteTodoClicked(id) {
    TodoDataService.deleteTodo(id).then(response => {
      this.setState({ message: `Delete of todo ${id} Successful` });
      this.refreshTodos();
    });
  }

  addTodoClicked() {
    this.props.history.push(`/todos/-1`);
  }

  updateTodoClicked(id) {
    this.props.history.push(`/todos/${id}`);
  }

  render() {
    return (
      <div className="row">
        <div className="col-lg-3">
          {this.state.todos.filter(todo => todo.itemStatus === 'TODO').map(todo => (
            <div className="card">
              <div className="container" key="{todo.id}">
                <div className="col-lg-12">
                  <div className="card-body">
                    <h4 className="card-title"> {todo.title} </h4>
                    <h5 className="text-dark card-subtitle mb-2">
                      {" "}
                      {todo.description}{" "}
                    </h5>
                    <p className="text-right d-flex align-items-center my-auto card-text">
                      {" "}
                      Created on:
                      {moment(todo.createdDate).format("YYYY-MM-DD")}{" "}
                    </p>
                  </div>
                </div>
              </div>

              <button
                className="btn btn-success"
                onClick={() => this.updateTodoClicked(todo.id)}
              >
                Update
              </button>
              <button
                className="btn btn-warning"
                onClick={() => this.deleteTodoClicked(todo.id)}
              >
                Delete
              </button>
            </div>
          ))}
        </div>

        <div className="col-lg-3 second-column">

          {this.state.todos.filter(todo => todo.itemStatus === 'ON_GOING').map(todo => (
              <div className="card">
                <div className="container" key="{todo.id}">
                  <div className="col-lg-12">
                    <div className="card-body">
                      <h4 className="card-title"> {todo.title} </h4>
                      <h5 className="text-dark card-subtitle mb-2">
                        {" "}
                        {todo.description}{" "}
                      </h5>
                      <p className="text-right d-flex align-items-center my-auto card-text">
                        {" "}
                        Created on:
                        {moment(todo.createdDate).format("YYYY-MM-DD")}{" "}
                      </p>
                    </div>
                  </div>
                </div>

                <button
                    className="btn btn-success"
                    onClick={() => this.updateTodoClicked(todo.id)}
                >
                  Update
                </button>
                <button
                    className="btn btn-warning"
                    onClick={() => this.deleteTodoClicked(todo.id)}
                >
                  Delete
                </button>
              </div>
          ))}
        </div>

        <div className="col-lg-3 third-column">

          {this.state.todos.filter(todo => todo.itemStatus === 'DONE').map(todo => (
              <div className="card">
                <div className="container" key="{todo.id}">
                <div className="completedOn">Completed on: 
                        {moment(todo.completedOn).format("YYYY-MM-DD")}</div>
                  <div className="col-lg-12"><br/>                  
                    <div className="card-body">
                      <h4 className="card-title"> {todo.title} </h4>
                      <h5 className="text-dark card-subtitle mb-2">
                        {" "}
                        {todo.description}{" "}
                      </h5>
                      <p className="text-right d-flex align-items-center my-auto card-text">
                        {" "}
                        Created on:
                        {moment(todo.createdDate).format("YYYY-MM-DD")}{" "}
                      </p>
                    </div>
                  </div>
                </div>

                <button
                    className="btn btn-success"
                    onClick={() => this.updateTodoClicked(todo.id)}
                >
                  Update
                </button>
                <button
                    className="btn btn-warning"
                    onClick={() => this.deleteTodoClicked(todo.id)}
                >
                  Delete
                </button>
              </div>
          ))}
        </div>
        <div className="row">
          <button className="btn btn-success1" onClick={this.addTodoClicked}>
            <i className="fa fa-plus-circle"></i>
            Add
          </button>
        </div>
      </div>
    );
  }
}

export default ListTodosComponent;