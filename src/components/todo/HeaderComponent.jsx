import React, { Component } from "react";
import { Link } from "react-router-dom";


class HeaderComponent extends Component {
  render() {
    return (
      <header>
        <div className="row main-header">
            <div className="col offset-lg-0">
                <div>
                    <div className="float-right float-right1"><i className="fa fa-check-circle float-right"></i>
                        <h1 className="float-right">DONE</h1>
                    </div>
                    <div className="justify-content-center div-2"><i className="fa fa-list-ul float-left"></i>
                        <h1>TODO</h1>
                    </div>
                    <div className="float-right float-right-2">
                        <h1 className="text-right float-left">ONGOING</h1><i className="fas fa-spinner"></i></div>
                </div>
            </div>
        </div>
      </header>
    );
  }
}

export default HeaderComponent;
