import React, { Component } from "react";
import moment from "moment";
import { Formik, Form, Field, ErrorMessage } from "formik";
import TodoDataService from "../../api/todo/TodoDataService.js";

class TodoComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      id: this.props.match.params.id,
      title: "",
      description: "",
      createdDate: moment(new Date()).format("YYYY-MM-DD"),
      completedOn: moment(new Date()).format("YYYY-MM-DD"),
      itemStatus: this.props.match.params.itemStatus
    };

    this.onSubmit = this.onSubmit.bind(this);
    this.validate = this.validate.bind(this);
  }

  componentDidMount() {
    if (this.state.id === -1) {
      return;
    }

    TodoDataService.retrieveTodo(this.state.id).then(response =>
      this.setState({
        title: response.data.title,
        description: response.data.description,
        createdDate: moment(response.data.createdDate).format("YYYY-MM-DD"),
        completedOn: moment(response.data.completedOn).format("YYYY-MM-DD"),
        itemStatus: response.data.itemStatus
      })
    );
  }

  validate(values) {
    let errors = {};

    if (!values.title) {
      errors.title = "Enter a Title";
    }

    if (!values.description) {
      errors.description = "Enter a Description";
    } else if (values.description.length < 5) {
      errors.description = "Enter atleast 5 Characters in Description";
    }

    if (!moment(values.createdDate).isValid()) {
      errors.createdDate = "Enter a valid created Date";
    }

        if (!values.itemStatus) {
      errors.itemStatus = "Choose item status";
    }

    return errors;
  }

  onSubmit(values) {
    let todo = {
      id: this.state.id,
      title: values.title,
      description: values.description,
      createdDate: values.createdDate,
      completedOn: values.completedOn,
      itemStatus: values.itemStatus
    };

    if (this.state.id === -1) {
      TodoDataService.createTodo(todo).then(() =>
        this.props.history.push("/todos")
      );
    } else {
      TodoDataService.updateTodo(this.state.id, todo).then(() =>
        this.props.history.push("/todos")
      );
    }

  }

  render() {
    let { title, description, createdDate, completedOn, itemStatus } = this.state;
    return (
      <div>
        <h1>Todo</h1>
        <div className="container container3">
          <Formik
            initialValues={{ title, description, createdDate, completedOn, itemStatus }}
            onSubmit={this.onSubmit}
            validateOnChange={false}
            validateOnBlur={false}
            validate={this.validate}
            enableReinitialize={true}
          >
            {props => (
              <Form>
                <ErrorMessage
                  name="title"
                  component="div"
                  className="alert alert-warning"
                />
                <ErrorMessage
                  name="description"
                  component="div"
                  className="alert alert-warning"
                />
                <ErrorMessage
                  name="createdDate"
                  component="div"
                  className="alert alert-warning"
                />
                <ErrorMessage
                  name="itemStatus"
                  component="div"
                  className="alert alert-warning"
                />

                <fieldset className="form-group">
                  <label>Title</label>
                  <Field className="form-control" type="text" name="title" />
                </fieldset>

                <fieldset className="form-group">
                  <label>Description</label>
                  <Field
                    className="form-control"
                    type="text"
                    name="description"
                  />
                </fieldset>

                <fieldset className="form-group">
                  <label>Created Date</label>
                  <Field
                    className="form-control"
                    type="date"
                    name="createdDate"
                  />
                </fieldset>

                <fieldset className="form-group">
                  <label>Status</label>
                  <Field as="select" className="form-control" name="itemStatus">
                    <option value="" selected disabled hidden>Choose Status</option>
                    <option value = "TODO">Todo</option>
                    <option value = "ON_GOING">In Progress</option>
                    <option value = "DONE">Done</option>
                  </Field>
                </fieldset>
                <button className="btn btn-success" type="submit">
                  Save
                </button>
              </Form>
            )}
          </Formik>
        </div>
      </div>
    );
  }
}

export default TodoComponent;